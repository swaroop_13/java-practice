package Practice02;

public class P001 {
	
	public static void main(String[]args) {
		
		StoreHashMap strMap = new StoreHashMap();
		StoreArrayList strList = new StoreArrayList();
		MethodsForExecution mtEx = new MethodsForExecution();
		DataForExecution dta = new DataForExecution();
		
		strMap.username();
		strMap.password();
		
		strList.elements();
		
		mtEx.signin(dta.username(), dta.password());
	}

}
